import sbt.ExclusionRule

name := "AETestTask"

scalaVersion := "2.12.3"
version := "0.1"

// The Typesafe repository
resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies += guice

// common
libraryDependencies ++= {
  val circeVersion = "0.8.0"

  Seq(
    "com.iheart" %% "ficus" % "1.4.3",
    "com.beachape" %% "enumeratum" % "1.5.12",
    "com.beachape" %% "enumeratum-circe" % "1.5.12",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"
      excludeAll ExclusionRule(organization = "io.circe")
  ) ++ Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % circeVersion)
}