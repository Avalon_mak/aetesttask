import io.circe.{Decoder, Encoder, HCursor, Json}

package object model {
  implicit val balanceDecoder: Decoder[CompanyBalance] = new Decoder[CompanyBalance] {
    final def apply(c: HCursor): Decoder.Result[CompanyBalance] =
      for {
        balance <- c.downField("balance").as[String]
        transactions <- c.downField("transactions").as[Seq[Transaction]]
        shortList <- c.downField("shortList").as[Seq[Transaction]]
      } yield {
        CompanyBalance(balance.toInt, transactions, shortList)
      }
  }

  implicit val transactionDecoder: Decoder[Transaction] = new Decoder[Transaction] {
    final def apply(c: HCursor): Decoder.Result[Transaction] =
      for {
        date <- c.downField("date").as[String]
        amount <- c.downField("amount").as[String]
        transactionType <- c.downField("transactionType").as[String]
        receiver <- c.downField("receiver").as[String]
      } yield {
        Transaction(date, amount, transactionType, receiver)
      }
  }

  implicit val balanceEncoder: Encoder[CompanyBalance] = new Encoder[CompanyBalance] {
    override def apply(f: CompanyBalance): Json = Json.obj(
      "balance" -> Json.fromInt(f.balance),
      "transactions" -> Json.fromValues(f.transactions.map(transactionEncoder(_))),
      "shortList" -> Json.fromValues(f.shortList.map(transactionEncoder(_)))
    )
  }

  implicit val transactionEncoder: Encoder[Transaction] = new Encoder[Transaction] {
    override def apply(a: Transaction): Json = Json.obj(
      "date" -> Json.fromString(a.date),
      "amount" -> Json.fromString(a.amount),
      "transactionType" -> Json.fromString(a.transactionType),
      "receiver" -> Json.fromString(a.receiver)
    )
  }
 }
