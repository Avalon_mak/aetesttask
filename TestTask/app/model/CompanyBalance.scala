package model

case class CompanyBalance(_balance: Integer, _transactions: Seq[Transaction], _shortList: Seq[Transaction]) {
  var balance = _balance
  var transactions: Seq[Transaction] = _transactions
  var shortList: Seq[Transaction] = transactions.drop(5)

  def addTransactionToList(transaction: Transaction): Seq[Transaction] = {
    transactions :+= transaction
    transactions
  }
}

case class Transaction(date: String, amount: String, transactionType: String, receiver: String)