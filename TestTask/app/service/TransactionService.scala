package service

import model.{CompanyBalance, Transaction}

trait TransactionService {
  def receiveTransaction(amount: Integer, sender: Integer): Option[Seq[Transaction]]
  def sentTransaction(amount: Integer, receiver: Integer): Option[CompanyBalance]
}
