package service
import java.util.Calendar

import model.{CompanyBalance, Transaction}

class TransactionServiceImpl(_companyBalance: CompanyBalance) extends TransactionService {
  private val companyBalance = _companyBalance

  override def receiveTransaction(amount: Integer, sender: Integer): Option[Seq[Transaction]] = {
    val date = Calendar.getInstance().getTime

    val newBalance = companyBalance.balance + amount
    companyBalance.balance = newBalance
    val transaction: Transaction = Transaction(date.toString, amount.toString, "RECEIVE", sender.toString)
    companyBalance.addTransactionToList(transaction)
    companyBalance.shortList = companyBalance.transactions.takeRight(5)
    Some(companyBalance.transactions)
  }

  override def sentTransaction(amount: Integer, receiver: Integer): Option[CompanyBalance] = {
    val date = Calendar.getInstance().getTime

    if (amount < companyBalance.balance) {
      val newBalance = companyBalance.balance - amount
      companyBalance.balance = newBalance
      val transaction: Transaction = Transaction(date.toString, amount.toString, "SEND", receiver.toString)
      companyBalance.addTransactionToList(transaction)
      companyBalance.shortList = companyBalance.transactions.takeRight(5)
      Some(companyBalance)
    } else {
      None
    }
  }
}
