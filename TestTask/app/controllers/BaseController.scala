package controllers

import javax.inject.Inject
import javax.inject._

import model.CompanyBalance
import play.api.mvc._
import service._
import io.circe.syntax.EncoderOps

import scala.util.Random

@Singleton
class BaseController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  private val companyBalance = CompanyBalance(1000000, Seq(), Seq())
  private val service: TransactionService = new TransactionServiceImpl(companyBalance)

  def homePageData(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] => {
      Ok(companyBalance.asJson.noSpaces)
    }
  }

  def sentTransaction(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] => {
      service.sentTransaction(request.body.asText.get.toInt, Random.nextInt (1000000) ) match {
          case Some (balance) => Ok(balance.asJson.noSpaces)
          case None => Forbidden(companyBalance.asJson.noSpaces)
      }
    }
  }

  def receiveTransaction(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] => {
      service.receiveTransaction(request.body.asText.get.toInt, Random.nextInt(1000000))
      Ok(companyBalance.asJson.noSpaces)
    }
  }

  def transactionsList(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] => {
      Ok(companyBalance.asJson.noSpaces)
    }
  }
}
