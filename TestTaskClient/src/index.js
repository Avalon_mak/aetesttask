import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

//import './index.evalorm.css';

import configureStore from './client/store';
import Actions from './client/actions';
import ManagerApp from './client/reducers';

import PageManager from './client/PageManager';
import PagesList from './client/PagesList';


const store = configureStore(
  ManagerApp,
  {/* predefined state, if needed*/
    currentPage: PagesList.HOME_PAGE
  }
);

// initial data fetching
store.dispatch(Actions.fetchHomePageData());

ReactDOM.render(
  <Provider store={store}>
    <PageManager />
  </Provider>,
  document.getElementById('root')
);
