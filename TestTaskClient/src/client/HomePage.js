import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './HomePageStyles.css'

import {Panel, Button, FormControl} from 'react-bootstrap';

class HomePage extends Component {

  valueToSent = (evt) => {
    let value = evt.target.value;

    this.props.onValueToSentChange(value);
  };

  valueToReceive = (evt) => {
    let value = evt.target.value;

    this.props.onValueToReceiveChange(value);
  };

  renderValidationMessage(type) {
    if (this.props.validationMessage !== undefined) {
      let typeFromState = this.props.validationMessage.field;
      if (typeFromState === type) {
        return (<Panel className='validation-error'>{this.props.validationMessage.message}</Panel>);
      }
    }
  };

  renderBalancePanel(balance) {

    return (
        <Panel className='balance-panel'>
          <h4>Current Balance: {balance}</h4>
        </Panel>
      );
  };

  renderButtonsPanel() {

    return (
        <Panel className='buttons-panel'>
          {this.renderValidationMessage('SENT')}
          <FormControl placeholder='Amount' type='number' onChange={this.valueToSent}/>
          <Button className='button' bsStyle='primary' onClick={() => this.props.onSentMoney()}>Send Payment</Button>
          {this.renderValidationMessage('RECEIVE')}
          <FormControl placeholder='Amount' type='number' onChange={this.valueToReceive}/>
          <Button className='button' bsStyle='primary' onClick={() => this.props.onReceiveMoney()}>Get Payment</Button>
        </Panel>
      );
  };

  renderShortHistoryPanel(lastTransactions) {
    let transactions;
    if (lastTransactions.length === 0) {
      transactions = <h4>There is no transactions for now</h4>
    } else {
      transactions = lastTransactions.map(transaction => this.renderRow(transaction));
    }

    return (
      <Panel>
        {transactions}
        <Button className='button' bsStyle='primary' onClick={() => this.props.onShowMore()}>Show More</Button>
      </Panel>
    );
  };

  renderRow(transaction) {
    let row;
    if (transaction.transactionType === 'SEND') {
      row = <Panel className='transaction-send'>
              <div>Amount: {transaction.amount}</div>
              <div>Date: {transaction.date}</div>
              <div>Receiver: {transaction.receiver}</div>
            </Panel>
    } else if (transaction.transactionType === 'RECEIVE') {
      row = <Panel className='transaction-receive'>
              <div>Amount: {transaction.amount}</div>
              <div>Date: {transaction.date}</div>
              <div>Sender: {transaction.receiver}</div>
            </Panel>
    }

    return (
        <div>
          {row}
        </div>
      );
  };

  render() {
    return (
      <div>
        {this.renderBalancePanel(this.props.balance)}
        {this.renderButtonsPanel()}
        {this.renderShortHistoryPanel(this.props.shortList)}
      </div>
    );
  };
};

HomePage.propTypes = {
  balance: PropTypes.string.isRequired,
  onSentMoney: PropTypes.func.isRequired,
  onReceiveMoney: PropTypes.func.isRequired,
  onValueToSentChange: PropTypes.func.isRequired,
  onValueToReceiveChange: PropTypes.func.isRequired,
  shortList: PropTypes.array.isRequired,
  onShowMore: PropTypes.func.isRequired,
  validationMessage: PropTypes.object
};

export default HomePage;