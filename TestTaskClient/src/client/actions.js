import HttpError from '../common/errors/HttpError.js';
import Validator from '../validation/Validator.js'

const HOME_PAGE_REQUEST_SENT = 'HOME_PAGE_REQUEST_SENT';
const HOME_PAGE_REQUEST_SUCCESSFUL = 'HOME_PAGE_REQUEST_SUCCESSFUL';
const HOME_PAGE_REQUEST_FAILED = 'HOME_PAGE_REQUEST_FAILED';

const MONEY_SENT_REQUEST_SENT = 'MONEY_SENT_REQUEST_SENT';
const MONEY_SENT_REQUEST_SUCCESSFUL = 'MONEY_SENT_REQUEST_SUCCESSFUL';
const MONEY_SENT_REQUEST_FAILED = 'MONEY_SENT_REQUEST_FAILED';

const MONEY_RECEIVE_REQUEST_SENT = 'MONEY_RECEIVE_REQUEST_SENT';
const MONEY_RECEIVE_REQUEST_SUCCESSFUL = 'MONEY_RECEIVE_REQUEST_SUCCESSFUL';
const MONEY_RECEIVE_REQUEST_FAILED = 'MONEY_RECEIVE_REQUEST_FAILED';

const SENT_FIELD_VALUE_CHANGE = 'SENT_FIELD_VALUE_CHANGE';
const RECEIVE_FIELD_VALUE_CHANGE = 'RECEIVE_FIELD_VALUE_CHANGE';

const SHOW_FULL_HISTORY_REQUEST_SENT = 'SHOW_FULL_HISTORY_REQUEST_SENT';
const SHOW_FULL_HISTORY_REQUEST_SUCCESSFUL = 'SHOW_FULL_HISTORY_REQUEST_SUCCESSFUL';
const SHOW_FULL_HISTORY_REQUEST_FAILED = 'SHOW_FULL_HISTORY_REQUEST_FAILED';

const BACK_TO_HOME_PAGE = 'BACK_TO_HOME_PAGE';

const VALIDATION_FAILED = 'VALIDATION_FAILED';


function emitHomePageRequestSent() {
  return {type: HOME_PAGE_REQUEST_SENT};
};

function emitHomePageRequestSuccessful(data) {
  return {type: HOME_PAGE_REQUEST_SUCCESSFUL, value: data};
};

function emitHomePageRequestFailed(error) {
  return {type: HOME_PAGE_REQUEST_FAILED, value: error};
};

function fetchHomePageData() {
  return (dispatch, getState) => {
    dispatch(emitHomePageRequestSent());

    fetch('/api/home/home/data')
    .then(
        response => {
          if (!response.ok) {
            throw new HttpError(response.status, response.statusText);
          }
          return response.json();
        }
    )
    .then(
        json => dispatch(emitHomePageRequestSuccessful(json))
    )
    .catch(
        error => {
          console.error('An error occurred.', error);
          dispatch(emitHomePageRequestFailed(error));
        }
    )
  }
}

function emitMoneySentRequestSent() {
  return {type: MONEY_SENT_REQUEST_SENT};
};

function emitMoneySentRequestSuccessful(data) {
  return {type: MONEY_SENT_REQUEST_SUCCESSFUL, value: data};
};

function emitMoneySentRequestFailed(error) {
  return {type: MONEY_SENT_REQUEST_FAILED, value: error};
};

function sentMoneyTransaction() {
  return (dispatch, getState) => {
      dispatch(emitMoneySentRequestSent());

      const amount = getState().valueToSent;

      fetch('/api/home/transaction/sends', {method: 'post', body: amount})
      .then(
          response => {
            if (!response.ok) {
              if (response.status === 403) {
                dispatch(emitValidationFailed("Operation reject for prevent negative balance", "SENT"));
              } else {
                throw new HttpError(response.status, response.statusText);
              }
            }
            return response.json();
          }
      )
      .then(
          json => dispatch(emitMoneySentRequestSuccessful(json))
      )
      .catch(
          error => {
            console.error('An error occurred.', error);
            dispatch(emitMoneySentRequestFailed(error));
          }
      )
    }
};

function emitMoneyReceiveRequestSent() {
  return {type: MONEY_RECEIVE_REQUEST_SENT};
};

function emitMoneyReceiveRequestSuccessful(data) {
  return {type: MONEY_RECEIVE_REQUEST_SUCCESSFUL, value: data};
};

function emitMoneyReceiveRequestFailed(error) {
  return {type: MONEY_RECEIVE_REQUEST_FAILED, value: error};
};

function receiveMoneyTransaction() {
  return (dispatch, getState) => {
      dispatch(emitMoneyReceiveRequestSent());

      const amount = getState().valueToReceive;

      fetch('/api/home/transaction/receives', {method:'post', body: amount})
      .then(
          response => {
            if(!response.ok){
              throw new HttpError(response.status, response.statusText);
            }
            return response.json();
          }
      )
      .then(
          json => dispatch(emitMoneyReceiveRequestSuccessful(json))
      )
      .catch(
          error => {
            console.error('An error occurred.', error);
            dispatch(emitMoneyReceiveRequestFailed(error));
          }
      )
    }
};

function emitSentFieldChange(value) {
  return {type: SENT_FIELD_VALUE_CHANGE, value: value}
};

function emitReceiveFieldChange(value) {
  return {type: RECEIVE_FIELD_VALUE_CHANGE, value: value}
};

function emitShowFullHistoryRequestSent() {
  return {type: SHOW_FULL_HISTORY_REQUEST_SENT}
};

function emitShowFullHistoryRequestSuccessful(history) {
  return {type: SHOW_FULL_HISTORY_REQUEST_SUCCESSFUL, value: history}
};

function emitShowFullHistoryRequestFailed(error) {
  return {type: SHOW_FULL_HISTORY_REQUEST_FAILED, value: error}
};

function showFullHistory() {
  return (dispatch, getState) => {
      dispatch(emitShowFullHistoryRequestSent());

      fetch('/api/home/transaction/records')
      .then(
          response => {
            if (!response.ok) {
              throw new HttpError(response.status, response.statusText);
            }
            return response.json();
          }
      )
      .then(
          json => dispatch(emitShowFullHistoryRequestSuccessful(json))
      )
      .catch(
          error => {
            console.error('An error occurred.', error);
            dispatch(emitShowFullHistoryRequestFailed(error));
          }
      )
    }
};

function emitBackToHome() {
  return {type: BACK_TO_HOME_PAGE};
};

function validateData(type) {
  return (dispatch, getState) => {
    const balance = getState().balance;
    let amount;
    if (type === 'SENT') {
      amount = getState().valueToSent;
    } else {
      amount = getState().valueToReceive;
    }
    let message = Validator.validate(amount, balance, type);
    if (message === null) {
      if (type === 'SENT') {
        dispatch(sentMoneyTransaction());
      } else {
        dispatch(receiveMoneyTransaction());
      }
    } else {
      dispatch(emitValidationFailed(message, type))
    }
  }
};

function emitValidationFailed(message, field) {
  return {type: VALIDATION_FAILED, value: message, field: field};
};

export const Actions = {
HOME_PAGE_REQUEST_SENT,
HOME_PAGE_REQUEST_SUCCESSFUL,
HOME_PAGE_REQUEST_FAILED,
fetchHomePageData,

MONEY_SENT_REQUEST_SENT,
MONEY_SENT_REQUEST_SUCCESSFUL,
MONEY_SENT_REQUEST_FAILED,
sentMoneyTransaction,

MONEY_RECEIVE_REQUEST_SENT,
MONEY_RECEIVE_REQUEST_SUCCESSFUL,
MONEY_RECEIVE_REQUEST_FAILED,
receiveMoneyTransaction,

SENT_FIELD_VALUE_CHANGE,
RECEIVE_FIELD_VALUE_CHANGE,
emitSentFieldChange,
emitReceiveFieldChange,

SHOW_FULL_HISTORY_REQUEST_SENT,
SHOW_FULL_HISTORY_REQUEST_SUCCESSFUL,
SHOW_FULL_HISTORY_REQUEST_FAILED,
showFullHistory,

BACK_TO_HOME_PAGE,
emitBackToHome,

VALIDATION_FAILED,
validateData,
emitValidationFailed
};

export default Actions