import Actions from './actions'
import PagesList from './PagesList';

export default function ManagerApp(state, action) {
  state = handleHomePage(state, action);
  state = handleSentTransaction(state, action);
  state = handleReceiveTransaction(state, action);
  state = handleFieldsChange(state, action);
  state = handleShowFullHistory(state, action);
  state = handleBack(state, action);
  state = handleProcessValidationMessage(state, action);
  return state;
}

function handleHomePage(state, action) {
  switch (action.type) {
    case Actions.HOME_PAGE_REQUEST_SENT:
      state.loading = true;
      return state;

    case Actions.HOME_PAGE_REQUEST_SUCCESSFUL:
      return {
        ...state,
        loading: false,
        currentPage: PagesList.HOME_PAGE,
        transactionsShortList: action.value.shortList,
        balance: action.value.balance
      };

    case Actions.HOME_PAGE_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    default: return state;
  }
}

function handleSentTransaction(state, action) {
  switch (action.type) {
    case Actions.MONEY_SENT_REQUEST_SENT:
      state.loading = true;
      return state;

    case Actions.MONEY_SENT_REQUEST_SUCCESSFUL:
      return {
        ...state,
        loading: false,
        currentPage: PagesList.HOME_PAGE,
        transactionsShortList: action.value.shortList,
        balance: action.value.balance
      };

    case Actions.MONEY_SENT_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    default: return state;
  }
}

function handleReceiveTransaction(state, action) {
  switch (action.type) {
    case Actions.MONEY_RECEIVE_REQUEST_SENT:
      state.loading = true;
      return state;

    case Actions.MONEY_RECEIVE_REQUEST_SUCCESSFUL:
      return {
        ...state,
        loading: false,
        currentPage: PagesList.HOME_PAGE,
        transactionsShortList: action.value.shortList,
        balance: action.value.balance
      };

    case Actions.MONEY_RECEIVE_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    default: return state;
  }
}

function handleFieldsChange(state, action) {
  switch (action.type) {
    case Actions.SENT_FIELD_VALUE_CHANGE:
    return {
      ...state,
      valueToSent: action.value
    };
    case Actions.RECEIVE_FIELD_VALUE_CHANGE:
    return {
      ...state,
      valueToReceive: action.value
    };
    default: return state;
  }
}

function handleShowFullHistory(state, action) {
  switch (action.type) {
    case Actions.SHOW_FULL_HISTORY_REQUEST_SENT:
      state.loading = true;
      return state;

    case Actions.SHOW_FULL_HISTORY_REQUEST_SUCCESSFUL:
      return {
        ...state,
        loading: false,
        currentPage: PagesList.TRANSACTIONS_HISTORY,
        transactions: action.value.transactions,
      };

    case Actions.SHOW_FULL_HISTORY_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        globalError: action.value
      };

    default: return state;
  }
}

function handleBack(state, action) {
  switch(action.type) {
    case Actions.BACK_TO_HOME_PAGE:
      return {
        ...state,
        loading: false,
        currentPage: PagesList.HOME_PAGE
      };

    default: return state;
  }
}

function handleProcessValidationMessage(state, action) {
  switch(action.type) {
    case Actions.VALIDATION_FAILED:
      return {
        ...state,
        loading: false,
        validation: {
            field: action.field,
            message: action.value
          },
      };
    default: return state;
  }
}