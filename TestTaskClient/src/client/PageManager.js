import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import HttpError from '../common/errors/HttpError.js';

import Actions from './actions';
import PagesList from './PagesList';
import HomePage from './HomePage.js';
import TransactionsHistory from './TransactionsHistory.js';

import { Grid, Row, Col, Panel, Label } from 'react-bootstrap';

class PageManager extends Component {

  render() {
    let header = 'Page Manager';
    let rootView;
    if (this.props.globalError) {
      rootView = <h4><Label bsStyle='danger'> There was an error: {this.props.globalError.message} </Label></h4>;
    } else if (this.props.currentPage === PagesList.HOME_PAGE) {
      rootView = <HomePage balance={this.props.balance}
          onSentMoney={this.props.onSentMoney}
          onReceiveMoney={this.props.onReceiveMoney}
          onValueToSentChange={this.props.onValueToSentChange}
          onValueToReceiveChange={this.props.onValueToReceiveChange}
          shortList={this.props.shortList}
          onShowMore={this.props.onShowMoreHistory}
          validationMessage={this.props.validationMessage}/>;

      header = <span><b>{'General'}</b></span>;
    } else if (this.props.currentPage === PagesList.TRANSACTIONS_HISTORY) {
      rootView = <TransactionsHistory
          onBack={this.props.onBack}
          history={this.props.history} />;
      header = <span><b>{'History'}</b></span>;
    }
    return (
        <Grid fluid={true}>
          <Row>
            <Col xs={8} md={8} xsOffset={2} mdOffset={2}>
              <Panel className='evalorm-main-panel' header={header}>
                {rootView}
              </Panel>
            </Col>
          </Row>
        </Grid>
    );
  };
};

PageManager.propsTypes = {
  currentPage: PropTypes.string,
  balance: PropTypes.string,
  globalError: PropTypes.oneOfType([PropTypes.instanceOf(HttpError)]),

  onSentMoney: PropTypes.func.isRequired,
  onReceiveMoney: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => {
  return {
    onSentMoney: () => {
      dispatch(Actions.validateData('SENT'));
    },
    onReceiveMoney: () => {
      dispatch(Actions.validateData('RECEIVE'));
    },
    onValueToSentChange: (value) => {
      dispatch(Actions.emitSentFieldChange(value))
    },
    onValueToReceiveChange: (value) => {
      dispatch(Actions.emitReceiveFieldChange(value))
    },
    onShowMoreHistory: () => {
      dispatch(Actions.showFullHistory())
    },
    onBack: () => {
      dispatch(Actions.emitBackToHome())
    }
  }
};

const mapStateToProps = state => {
  return {
    currentPage: state.currentPage,
    globalError: state.globalError,
    balance: state.balance,
    shortList: state.transactionsShortList,
    history: state.transactions,
    validationMessage: state.validation
  }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageManager);
