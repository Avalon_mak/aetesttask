import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './HomePageStyles.css'

import {Panel, Button} from 'react-bootstrap';

class TransactionsHistory extends Component {

  renderHistoryPanel(history) {
    let transactions;
    if (history.length === 0) {
      transactions = <h4>There is no transactions for now</h4>
    } else {
      transactions = history.map(transaction => this.renderRow(transaction));
    }

    return (
      <Panel>
        {transactions}
        <Button className='button' bsStyle='primary' onClick={() => this.props.onBack()}>Back</Button>
      </Panel>
    );
  };

  renderRow(transaction) {
    let row;
    if (transaction.transactionType === 'SEND') {
      row = <Panel className='transaction-send'>
              <div>Amount: {transaction.amount}</div>
              <div>Date: {transaction.date}</div>
              <div>Receiver: {transaction.receiver}</div>
            </Panel>
    } else if (transaction.transactionType === 'RECEIVE') {
      row = <Panel className='transaction-receive'>
              <div>Amount: {transaction.amount}</div>
              <div>Date: {transaction.date}</div>
              <div>Sender: {transaction.receiver}</div>
            </Panel>
    }

    return (
        <div>
          {row}
        </div>
      );
  };

  render() {
    return (
      <div>
        {this.renderHistoryPanel(this.props.history)}
      </div>
    );
  };
};

TransactionsHistory.propTypes = {
  history: PropTypes.array.isRequired,
  onBack: PropTypes.func.isRequired
};

export default TransactionsHistory;