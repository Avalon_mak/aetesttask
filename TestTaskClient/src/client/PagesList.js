const HOME_PAGE = 'HOME_PAGE'
const TRANSACTIONS_HISTORY = 'TRANSACTIONS_HISTORY'

export const PagesList = {
  HOME_PAGE,
  TRANSACTIONS_HISTORY
};

export default PagesList